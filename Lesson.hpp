#include <String>
#include <vector>
#include <map>



using namespace std;

class Lesson {
    public:
        Lesson(int, const int, const int, const int, vector<string>, vector<string>, vector<string>, vector<string>, vector<string>);
        int getId();
        map<string, vector<string>> getTimes();
        int getConfig(){return config; }
        int getStart(){return start; }
        int getLength(){return length; }
        string getDays(){return days; }
        string getWeeks(){return weeks; }
        int getRoom(){return room; }
        int getPenalty(){return penalty; }

        bool setTime(int, int, string, string);
        void setRoom(int);
        void setPenalty(int);
        void addStudent(int);
    private:
        int id;
        int course_id;
        int config;
        int subpart;

        vector<int> students;

        int room;

        int start;
        int length;
        string days;
        string weeks;
        int penalty;

        
        vector<string> poss_days;
        vector<string> poss_starts;
        vector<string> poss_length;
        vector<string> poss_week;
        vector<string> poss_time_pen;
           
};