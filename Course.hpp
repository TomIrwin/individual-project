#include <String>
#include <vector>
#include <map>

#include "Lesson.cpp"

using namespace std;

class Course {
    public:
        Course(int, map<int, map<int, map<int, map<string, vector<string>>>>> , vector<int>);
        map<int, map<int, map<int, map<string, vector<string>>>>> getConfigs();
        int getId();
        vector<Lesson>& getLessons();
        vector<int> getStudents();
        bool addStudent(int);

        void setLessons(vector<Lesson>);
    private:
        int id;
        map<int, map<int, map<int, map<string, vector<string>>>>> configs;
        vector<Lesson> lessons;
        vector<int> students;

        
        
        
};