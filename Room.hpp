#include <iostream>
#include <String>
#include <vector>
#include <map>
#include <algorithm>

class Room {
    public:
        Room(int, std::vector<int>, std::vector<int>, std::vector<std::string>, std::vector<std::string>);
        bool isAvailable(int, int, std::string, std::string);
        bool setTime(int, int, std::string, std::string);
        void removeTime(int, int, std::string, std::string);
        std::map<int, std::map<int, std::vector<int>>> getStartTimes();
        std::map<int, std::map<int, std::vector<int>>> getLengthTimes();
        
    private:
        int id;
        int capacity;
        
        std::vector<int> unavailable_start;
        std::vector<int> unavailable_length;
        std::vector<std::string> unavailable_days;
        std::vector<std::string> unavailable_weeks;

        

        std::map<int, std::map<int, std::vector<int>>> class_start;
        std::map<int, std::map<int, std::vector<int>>> class_length;
        std::map<int, std::map<int, std::vector<int>>> class_id;
        
};