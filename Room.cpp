#include "Room.hpp"


Room::Room(int _id,  std::vector<int> _unavailable_start, std::vector<int> _unavailable_length, std::vector<std::string> _unavailable_days, std::vector<std::string> _unavailable_weeks){

	for (int i = 0; i < _unavailable_start.size(); i++){
		//std::cout << " intialiser " << _unavailable_start[i] << " " << _unavailable_length[i] << std::endl;
	} 
	id = _id;
    
    unavailable_start = _unavailable_start;
    unavailable_length = _unavailable_length;
    unavailable_days = _unavailable_days;
    unavailable_weeks = _unavailable_weeks;

    
    
    std::vector<int> current_day_start, current_day_length, current_day_class;
    std::map<int, std::vector<int>> current_week_len, current_week_start, current_class_week;
    if (unavailable_weeks.size() > 0){
		for (int week = 0; week < unavailable_weeks[0].length(); week++){
			for (int day = 0; day < unavailable_days[0].length(); day++){ 
					for (int i = 0; i < unavailable_weeks.size(); i++){
						if (unavailable_weeks[i][week] == '1'){
							if (unavailable_days[i][day] == '1'){
								current_day_start.push_back(unavailable_start[i]);
								current_day_length.push_back(unavailable_length[i]);
								
							}
						}
					current_week_start.insert(std::pair<int,std::vector<int>>(day, current_day_start));
					current_week_len.insert(std::pair<int,std::vector<int>>(day, current_day_length));     
					current_class_week.insert(std::pair<int,std::vector<int>>(day, current_day_class));
					current_day_start.clear();
					current_day_length.clear();
				}
			}
			class_start.insert(std::pair<int, std::map<int, std::vector<int>>>(week, current_week_start));
			class_length.insert(std::pair<int, std::map<int, std::vector<int>>>(week, current_week_len));
			class_id.insert(std::pair<int, std::map<int, std::vector<int>>>(week, current_class_week)); 
			current_week_start.clear();
			current_week_len.clear();
			current_class_week.clear();
		}
    }
    
}

//function to determine the availability of a room at a given time, days and weeks. returning true if the room is available at these times and false if otherwise 
bool Room::isAvailable(int start, int length, std::string days, std::string weeks){
	std::vector<int> current_starts, current_lengths;
	int proposed_start = start, proposed_end = start + length, lesson_start, lesson_end;
	for (int week = 0; week < weeks.length(); week++){
		if (weeks[week] == '1'){
			for (int day = 0; day < days.length(); day++){
				if (days[day] == '1'){
					current_starts = class_start[week][day];
					current_lengths = class_length[week][day];
					for (int i = 0; i < current_starts.size(); i++){
						lesson_start = current_starts[i];
						lesson_end = current_starts[i]+current_lengths[i];
						if ((start < (current_starts[i] + current_lengths[i]) && start > current_starts[i]) || ((start + length) > current_starts[i] && (start + length) < (current_starts[i] + current_lengths[i])) || (start <= current_starts[i] && (start + length) >=(current_starts[i] + current_lengths[i])) || (start >= current_starts[i] && (start + length) <=(current_starts[i] + current_lengths[i])) ){
							return false;
						}
						
					}					
				}
			}
		}
	}
	return true;
}

//function to set the room to unavailable at a given time, days and weeks.
bool Room::setTime(int start, int length, std::string days, std::string weeks){
	for (int week = 0; week < weeks.length(); week++){
		if (weeks[week] == '1'){
			for (int day = 0; day < days.length(); day++){
				if (days[day] == '1'){
					class_start[week][day].push_back(start);
					class_length[week][day].push_back(length);			
				}
			}
		}
	}
	return true;
}

//removes a time and sets the given time to available so another class can take this time
void Room::removeTime(int start, int length, std::string days, std::string weeks){
	for (int week = 0; week < weeks.length(); week++){
		if (weeks[week] == '1'){
			for (int day = 0; day < days.length(); day++){
				if (days[day] == '1'){
					for(int i = 0; i < class_start[week][day].size(); i++){
						if (class_start[week][day][i] == start){
							class_start[week][day].erase(class_start[week][day].begin() +i);
							class_length[week][day].erase(class_length[week][day].begin() +i);
						}
					}
				}
			}
		}
	}
}
std::map<int, std::map<int, std::vector<int>>> Room::getStartTimes(){return class_start;}

std::map<int, std::map<int, std::vector<int>>> Room::getLengthTimes(){return class_length;}