#include "Course.hpp"

#include <iostream>
#include <map>

Course::Course(int _id, map<int, map<int, map<int, map<string, vector<string>>>>>  _configs, vector<int> _students){
    id = _id;
    configs = _configs;
    students = _students;

    //generate lesson instances with the configuration data passed for this course.
    for (map<int, map<int, map<int, map<string, vector<string>>>>>::iterator config_id = configs.begin(); config_id != configs.end(); ++config_id){
        for ( map<int, map<int, map<string, vector<string>>>>::iterator subpart_id = config_id->second.begin(); subpart_id != config_id->second.end(); ++subpart_id){
            for ( map<int, map<string, vector<string>>>::iterator class_id = subpart_id->second.begin(); class_id != subpart_id->second.end(); ++class_id){
                lessons.push_back(Lesson(id, config_id->first, subpart_id->first, class_id->first, class_id->second["days"], class_id->second["start"],class_id->second["length"],  class_id->second["week"],  class_id->second["time_pen"]));
            }
        }
    }
}

map<int, map<int, map<int, map<string, vector<string>>>>> Course::getConfigs(){ return configs;}

vector<Lesson>& Course::getLessons(){ return lessons;}

int Course::getId(){ return id;}
vector<int> Course::getStudents(){ return students; }
bool Course::addStudent(int student){
    students.push_back(student);
    return true;
}

void Course::setLessons(vector<Lesson> l){
    lessons = l;
}