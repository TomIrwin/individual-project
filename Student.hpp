#include <String>
#include <vector>

class Student {
    public:
        Student(int, std::vector<int>);
        int getId();
        std::vector<int> getCourses();
        bool assignConfigs();
        void addLesson(int,int,std::string, std::string);
         
    private:
        int id;
        std::vector<int> courses;
        std::vector<std::vector<std::vector<int>>> class_starts;
        std::vector<std::vector<std::vector<int>>> class_lengths;
        
};