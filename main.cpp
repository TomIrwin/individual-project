#include "Room.cpp"
#include "Student.cpp"
#include "Course.cpp"

#include <iostream>
#include <fstream>

#include <map>
#include <time.h>


#include "tinyxml2.h"
#include "tinyxml2.cpp"

using namespace std;


using namespace tinyxml2;

//xml_document<> doc;

vector<Course> courses;
vector<Room> rooms;
vector<Student> students;

int total_penalty =-1;
vector<int> neighbour_pen;
vector<vector<Lesson>> neighbour_lessons;
vector<vector<Room>> neighbour_rooms;

int totalit = 0;

map<int, map<int, map<int, map<string, vector<string>>>>> test_configs;

void load_file(string filename){
    
    tinyxml2::XMLDocument doc;
    
	doc.LoadFile( "test.xml" );

    //tinyxml produces a tree structure to represent the XML file

    XMLPrinter printer;
    XMLNode * root = doc.FirstChild();
    const XMLAttribute * current_attr;
    string current_str;

    root = root->NextSibling()->NextSibling();
    //Traverse the XML tree extracting the important data
    for (const XMLNode * child_node = root->FirstChild(); child_node; child_node = child_node->NextSibling()){
        
        //Load Rooms
        if (strcmp(child_node->Value(), "rooms") == 0){
            for (const XMLNode *  i = child_node->FirstChild(); i; i = i->NextSibling()){
                vector<string> unavailable_days;
                vector<string> unavailable_weeks;
                vector<int> unavailable_start;
                vector<int> unavailable_length;

                vector<int> unavailable_penalty;

                vector<int> travel_time;
                vector<int> travel_room;

                //Find Unavailable times for this room
                for (const XMLNode *  j = i->FirstChild(); j; j = j->NextSibling()){
                    current_attr = j->ToElement()->FirstAttribute();
                    current_str = current_attr->Value();
                    unavailable_days.push_back(current_str);

                    current_attr = current_attr->Next();
                    unavailable_start.push_back(atoi(current_attr->Value()));

                    current_attr = current_attr->Next();
                    unavailable_length.push_back(atoi(current_attr->Value()));

                    current_attr = current_attr->Next();
                    current_str = current_attr->Value();
                    unavailable_weeks.push_back(current_str);
                    
                    current_attr = current_attr->Next();
                    if (current_attr != 0){
                        unavailable_penalty.push_back(atoi(current_attr->Value()));
                    }
                }
                //generate a Room instance and add it to the rooms vector
                rooms.push_back(Room(atoi(i->ToElement()->Attribute("id")),  unavailable_start, unavailable_length, unavailable_days,unavailable_weeks));
            }
        } else if (strcmp(child_node->Value(), "courses") == 0){ //Load courses
            for (const XMLNode *  course = child_node->FirstChild(); course; course = course->NextSibling()){
                map<int, map<int, map<int, map<string, vector<string>>>>> _configs; //map within a map within a map within a map
                for (const XMLNode * config = course->FirstChild(); config; config = config->NextSibling()){
                    map<int, map<int, map<string, vector<string>>>> subparts;
                    for (const XMLNode * subpart = config->FirstChild(); subpart; subpart = subpart->NextSibling()){
                        map<int, map<string, vector<string>>> _classes;
                        for (const XMLNode * _class = subpart->FirstChild(); _class; _class = _class->NextSibling()){
                            map<string, vector<string>> class_map;
                            
                            vector<string> days;
                            vector<string> start;
                            vector<string> length;
                            vector<string> week;
                            vector<string> time_pen;

                            for (const XMLNode * element = _class->FirstChild(); element; element = element->NextSibling()){
                                if (strcmp(element->Value(), "time") == 0){
                                    
                                    //extract the important information from the XML tree
                                    current_attr = element->ToElement()->FirstAttribute();
                                    days.push_back(current_attr->Value());

                                    current_attr = current_attr->Next();
                                    start.push_back(current_attr->Value());

                                    current_attr = current_attr->Next();
                                    length.push_back(current_attr->Value());

                                    current_attr = current_attr->Next();
                                    week.push_back(current_attr->Value());

                                    current_attr = current_attr->Next();
                                    time_pen.push_back(current_attr->Value());
                                }
                            }
                            //generate a map with the possible time data
                            class_map.insert(std::pair<string, vector<string>>("days", days));
                            class_map.insert(std::pair<string, vector<string>>("start", start));
                            class_map.insert(std::pair<string, vector<string>>("length", length));
                            class_map.insert(std::pair<string, vector<string>>("week", week));
                            class_map.insert(std::pair<string, vector<string>>("time_pen", time_pen));

                            _classes.insert(std::pair<int, map<string, vector<string>>>(atoi(_class->ToElement()->Attribute("id")), class_map));
                        }
                    subparts.insert(std::pair<int, map<int, map<string, vector<string>>>>(atoi(subpart->ToElement()->Attribute("id")), _classes));
                    }
                _configs.insert(std::pair<int, map<int, map<int, map<string, vector<string>>>>>(atoi(config->ToElement()->Attribute("id")), subparts));
                }
                vector<int> empty_vector;
                test_configs = _configs;
                //generate Course instance and add to courses vector
                courses.push_back(Course(atoi(course->ToElement()->Attribute("id")), _configs, empty_vector));   
            }
        
        } else if (strcmp(child_node->Value(), "students") == 0){ //Load Students
            std::vector<int> student_classes;
            for (const XMLNode *  student = child_node->FirstChild(); student; student = student->NextSibling()){

                for (const XMLNode *  _class = student->FirstChild(); _class; _class = _class->NextSibling()){
                    student_classes.push_back(atoi(_class->ToElement()->FirstAttribute()->Value()));
                }
                students.push_back(Student(atoi(student->ToElement()->Attribute("id")), student_classes));
                student_classes.clear();
            }
        }
    }
    
}

//function to assign a time and room to a class 
void assign_time(int course, int lesson,map<string, vector<string>> current_times){
    for (int k = 0; k < rooms.size(); k++){
        for (int j = 0; j < current_times["starts"].size(); j++){  
            if (rooms[k].isAvailable(stoi(current_times["starts"][j]), stoi(current_times["lengths"][j]), current_times["days"][j], current_times["weeks"][j]) == true){
                rooms[k].setTime(stoi(current_times["starts"][j]), stoi(current_times["lengths"][j]), current_times["days"][j], current_times["weeks"][j]);
                courses[course].getLessons()[lesson].setTime(stoi(current_times["starts"][j]), stoi(current_times["lengths"][j]), current_times["days"][j], current_times["weeks"][j]);
                courses[course].getLessons()[lesson].setRoom(k + 1);
                courses[course].getLessons()[lesson].setPenalty(stoi(current_times["penalty"][j]));
                total_penalty += stoi(current_times["penalty"][j]); 
                return;
             }
                
        }
    }
}

//find the basic feasible solution to start the search from.
void initial_solution(){
    vector<Lesson> current_lessons;
    
    //assign students to courses
    std::vector<int> current_courses;
    for (int student = 0; student < students.size(); student++){
        current_courses = students[student].getCourses();
        for (int i = 0; i < current_courses.size(); i++){
            courses[current_courses[i]-1].addStudent(students[student].getId());
        }
    }

    //assign the first possible time to each class in the problem
    map<string, vector<string>> current_times;
    vector<int> current_students;
    int current_config;
    for (int course = 0; course < courses.size(); course++){
        current_lessons = courses[course].getLessons();
        for (int i = 0; i < current_lessons.size(); i++){
            current_times = current_lessons[i].getTimes();
            assign_time(course, i, current_times);
            
        }      
    }
}

//function that will generate neighbours for the current solution and return the neighbour with the lowest total penalty.
int find_best_neighbour(){
    vector<int> course_ids;
    int rand_neighbour, current_lesson_pen, best_neighbour_pen, best_neighbour_index;
    
    
    for (int course = 0; course < courses.size(); course++){
        map<int, map<int, map<int, map<string, vector<string>>>>> current_configs; 
        vector<Lesson>& current_lessons = courses[course].getLessons();
        for (int i = 0; i < current_lessons.size(); i++){  
            //get details of the current lesson       
            map<string, vector<string>> current_t = current_lessons[i].getTimes();
            current_lesson_pen = current_lessons[i].getPenalty();
            //find random neighbours
            srand(time(0));
            //generate x number of neighbours
            for (int j = 0; j < 20; j++){
                rand_neighbour = rand() % current_t["starts"].size();
                for (int k = rand() & rooms.size(); k < rooms.size(); k++){
                    if (rooms[k].isAvailable(stoi(current_t["starts"][rand_neighbour]), stoi(current_t["lengths"][rand_neighbour]), current_t["days"][rand_neighbour], current_t["weeks"][rand_neighbour]) == true){
                        neighbour_pen.push_back(total_penalty - current_lesson_pen + stoi(current_t["penalty"][rand_neighbour]));
                        //set time and store
                        neighbour_lessons.push_back(current_lessons);
                        course_ids.push_back(course + 1);
                        neighbour_rooms.push_back(rooms);
                        neighbour_rooms[neighbour_rooms.size()-1][current_lessons[i].getRoom() - 1].removeTime(current_lessons[i].getStart(), current_lessons[i].getLength(), current_lessons[i].getDays(),current_lessons[i].getWeeks());
                        neighbour_lessons[neighbour_lessons.size() - 1][i].setTime(stoi(current_t["starts"][rand_neighbour]), stoi(current_t["lengths"][rand_neighbour]), current_t["days"][rand_neighbour], current_t["weeks"][rand_neighbour]);
                        neighbour_rooms[neighbour_rooms.size() - 1][k].setTime(stoi(current_t["starts"][rand_neighbour]), stoi(current_t["lengths"][rand_neighbour]), current_t["days"][rand_neighbour], current_t["weeks"][rand_neighbour]);
                        //neighbour has been stored
                    }
                }
            }
        }
    }

    //determine the best neighbour for this current solution
    best_neighbour_pen = neighbour_pen[0];
    best_neighbour_index = 0;
    for (int j = 0; j < neighbour_lessons.size(); j++){
        if (neighbour_pen[j] < best_neighbour_pen)
            best_neighbour_index = j;
            best_neighbour_pen = neighbour_pen[j];
    }

    //set the current solution to the best neighbour to the old solution.
    rooms = neighbour_rooms[best_neighbour_index];
    courses[course_ids[best_neighbour_index] - 1].setLessons(neighbour_lessons[best_neighbour_index]);
    neighbour_lessons.clear();
    neighbour_pen.clear();

    return best_neighbour_pen;
}

void find_solution(){
    //find the basic feasible solution
    initial_solution();
    totalit ++ ;
    
    //start the local search
    int new_penalty = find_best_neighbour();
    //iterate while better solutions are being found
    while (new_penalty < total_penalty){
        total_penalty = new_penalty;
        new_penalty = find_best_neighbour();
        totalit++;
    }
    
    //assign students to configarations 
    vector<int> current_students;
    int current_config;
    for (int course = 0; course < courses.size(); course++){
        vector<Lesson>& current_lessons = courses[course].getLessons();
        current_students = courses[course].getStudents();
        for(int i = 0; i < current_students.size(); i++){
            //assign student to config 1
            current_lessons = courses[course].getLessons();
            current_config = current_lessons[0].getConfig();
            for (int i = 0; i < current_lessons.size(); i++){
                 if (current_lessons[i].getConfig() == current_config){
                    //add student to this class
                    current_lessons[i].addStudent(current_students[i]);
                    //add class to student time table
                    
                }
            }
         }
    }
}

//UNIT TESTS

bool assert_equal(int a, int b){
    if (a == b){
        return true;
    }
    return false;
}

bool test_courses(){
    vector<Course> test_courses;
    vector<int> test_students;
    test_students.push_back(1); 
    test_students.push_back(2); 
    
    test_courses.push_back(Course(1, test_configs, test_students));
    if (assert_equal(test_courses[0].getId(), 1) == false){
        return false;
    }
    if (assert_equal(test_courses[0].getStudents().size(), 2)){
        return true;
    }
    return false;
}

bool test_students(){
    vector<Student> test_students;
    vector<int> test_courses;
    test_courses.push_back(1);
    test_courses.push_back(2);
    test_students.push_back(Student(1, test_courses));
    if (assert_equal(test_students[0].getId(), 1) == true){
        return true;
    }
    return false;
}

bool test_rooms(){
    vector<Room> test_rooms;
    vector<int> test_unav;
    vector<string> test_str;
    test_rooms.push_back(Room(1,test_unav, test_unav, test_str, test_str));
    if (test_rooms[0].isAvailable(90,10,"1100000", "1111111111111") == false){
        return false;
    }
    if (test_rooms[0].setTime(90,10,"1100000", "1111111111111") == true){
        return true;
    }
    return false;
}

bool unit_tests(){
    if (test_courses() == false){
        return false;
    }
    if (test_students() == false){
        return false;
    }
    if (test_rooms() == false){
        return false;
    }
    return true;
}

int main(){
    
    std::cout << "Starting" << endl;

    //Load the test data
    load_file("wbg-fal10__1_.xml");
    
    //Run the Unit tests
    std::cout << "Running unit tests" << endl;
    if (unit_tests() == true){
        cout << "Unit tests passed" << endl;
    }
    else{
        cout << "Unit tests failed" << endl;
    }

    //Start the local search
    find_solution();
 
    cout << "total penalty: " << total_penalty << endl << "number of iterations: "  << totalit << endl;
}