#include "Lesson.hpp"

Lesson::Lesson(int _course, const int _config, const int _subpart, const int class_id, vector<string> _poss_days,  vector<string> _poss_start, vector<string> _poss_length, vector<string> _poss_week, vector<string> _poss_time_pen){
    id = class_id;
    course_id = _course;
    config = _config;
    subpart = _subpart;

    
    poss_days = _poss_days;
    poss_starts = _poss_start;
    poss_length = _poss_length;
    poss_week = _poss_week;
    poss_time_pen = _poss_time_pen;

}

int Lesson::getId(){ return id;}


map<string, vector<string>> Lesson::getTimes(){
    map<string, vector<string>> times;
    times.insert(std::pair<string, vector<string>>("starts", poss_starts));
    times.insert(std::pair<string, vector<string>>("lengths", poss_length));
    times.insert(std::pair<string, vector<string>>("days", poss_days));
    times.insert(std::pair<string, vector<string>>("weeks", poss_week));
    times.insert(std::pair<string, vector<string>>("penalty", poss_time_pen));
    return times;
    
}

bool Lesson::setTime(int _start, int _length, string _days, string _weeks){
    start = _start;
    length = _length;
    days = _days;
    weeks = _weeks;
    return true;
}

void Lesson::setRoom(int r){
    room = r;
}

void Lesson::setPenalty(int p){
    penalty = p;
}
void Lesson::addStudent(int s){
    students.push_back(s);
}
