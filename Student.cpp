#include "Student.hpp"

Student::Student(int _id, std::vector<int> _courses){
    id = _id;
    courses = _courses;
    std::vector<std::vector<std::vector<int>>> class_starts;
    std::vector<std::vector<std::vector<int>>> class_lengths;

    std::vector<std::vector<int>> current_week_start, current_week_length;
    std::vector<int> current_day_start, current_day_length;

    //generates a student timetable
    for (int week = 0; week < 13; week++){
        for (int day = 0; day < 7; day++){
            current_week_start.push_back(current_day_start);
            current_week_length.push_back(current_day_length);
        }
        class_starts.push_back(current_week_start);
        class_lengths.push_back(current_week_length);
    }
}

int Student::getId(){return id;}

std::vector<int> Student::getCourses(){return courses;}

bool Student::assignConfigs(){
    
    return true;
}

//add a lesson to the students timetable.
void Student::addLesson(int start, int length, std::string days, std::string weeks){
	int insert_index;
	
	for (int week = 0; week < weeks.length(); week++){
		if (weeks[week] == '1'){
			for (int day = 0; day < days.length(); day++){
				if (days[day] == '1'){					
					insert_index = 0;				
					class_starts[week][day].push_back(start);
					class_lengths[week][day].push_back(length);

				}
			}
		}
	}
}